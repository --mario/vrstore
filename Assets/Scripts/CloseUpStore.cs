﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Productdata
{
    public string productName;
}

public class CloseUpStore : CloseUpBehaviour {

    public GameObject cart;
    public GameObject cartContent;
    public GameObject rowPrefab;

    private List<Productdata> allData = new List<Productdata>();

    /// <summary>
    /// add the product to the cart
    /// </summary>
    public void AddToCart()
    {
        if (!IsProductSelected())
            return;

        foreach(Productdata data in allData)
        {
            if (closeView.GetChild(0).name == data.productName)
                return;
        }

        Productdata pd = new Productdata();
        pd.productName = closeView.GetChild(0).name;
        allData.Add(pd);

        GameObject  go = Instantiate(rowPrefab, cartContent.transform);
        go.transform.GetChild(0).GetComponent<Text>().text = pd.productName;

        cart.SetActive(true);
    }

    /// <summary>
    /// hide the cart
    /// </summary>
    public void HideCart()
    {
        cart.SetActive(false);
    }
}
