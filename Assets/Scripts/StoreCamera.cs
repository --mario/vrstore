﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreCamera : CameraBehaviour {

    public CloseUpStore closeUp;

    void Start()
    {
        _mainCam = GetComponent<Camera>();
        canMakeSelection = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentObject != null)
            Hovered();
    }

    void Hovered()
    {
        if (currentObject.tag == "Icon")
        {
            CommonHoverOptions(currentObject);
            ExtendedHoverOptions(currentObject);
        }
        else if (currentObject.tag == "Product" && _isClick && canMakeSelection)
            closeUpScript.ProductSelected(currentObject);
    }

    void ExtendedHoverOptions(GameObject button)
    {
        switch (button.name)
        {
            case "addToCart":
                {
                    if (_isClick)
                        closeUp.AddToCart();
                }
                break;
            case "return":
                {
                    if (_isClick)
                        closeUp.HideCart();
                }
                break;

        }
    }
}
