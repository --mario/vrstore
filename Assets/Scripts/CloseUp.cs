﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseUp : CloseUpBehaviour {
    
    public GameObject[] allNotifcaions;
    private int index = 0;

 
    /// <summary>
    /// change the user notifications
    /// </summary>
    public void ChangeMessage()
    {
        allNotifcaions[index].SetActive(false);

        if (index < allNotifcaions.Length - 1)
        {
            index++;
            allNotifcaions[index].SetActive(true);
        }
        else
            playerCamera.GetComponent<TutorialCamera>().canMakeSelection = true;
    }
    
}
