﻿using UnityEngine;

public class ChangeScene : MonoBehaviour {
    
    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider.gameObject.name == "Exit")
            UnityEngine.SceneManagement.SceneManager.LoadScene("Store");
    }
}
