﻿using UnityEngine;
using UnityEngine.UI;

public class CameraBehaviour : MonoBehaviour {

    public CloseUpBehaviour closeUpScript;
    public Image reticle;
    [HideInInspector]
    public bool canMakeSelection;
    [Range(1f, 2f)]
    public float selectionTime;

    protected Camera _mainCam;
    private RaycastHit _hit;
    protected bool _isClick = false;
    protected GameObject currentObject;
    private float currentTime = 0f;
    private int layerMask = 1 << 9;

    protected void FixedUpdate()
    {
        _isClick = false;
        currentObject = null;

        if (Input.GetButtonUp("Fire1") || reticle.fillAmount == 1)
        {
            _isClick = true;
            currentTime = 0;
        }

        if (Physics.Raycast(new Ray(_mainCam.transform.position, _mainCam.transform.forward), out _hit, 100f, layerMask))
        {
            currentObject = _hit.collider.gameObject;

            if (currentObject.tag != "Untagged")
                currentTime += Time.deltaTime;
            else
                currentTime = 0;
        }
        else
            currentTime = 0;

        reticle.fillAmount = currentTime/ selectionTime;
    }

    protected void CommonHoverOptions(GameObject button)
    {
        switch (button.name)
        {
            case "zoomIn":
                {
                    if (_isClick)
                        closeUpScript.ScaleProduct(true);
                }
                break;
            case "zoomOut":
                {
                    if (_isClick)
                        closeUpScript.ScaleProduct(false);
                }
                break;
            case "rotateLeft":
                {
                    closeUpScript.RotateProduct(Vector3.up);
                }
                break;
            case "rotateRight":
                {
                    closeUpScript.RotateProduct(Vector3.down);
                }
                break;
            case "rotateUp":
                {
                    closeUpScript.RotateProduct(Vector3.left);
                }
                break;
            case "rotateDown":
                {
                    closeUpScript.RotateProduct(Vector3.right);
                }
                break;
            case "return":
                {
                    if (_isClick)
                        closeUpScript.ReturnProduct();
                }
                break;
        }
    }
}
