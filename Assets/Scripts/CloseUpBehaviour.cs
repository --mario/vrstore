﻿using UnityEngine;

public class CloseUpBehaviour : MonoBehaviour {

    public Transform closeView;
    public GameObject option;
    public Camera playerCamera;
    public CharacterController player;

    private Vector3 _oldProductPosition;
    private Quaternion _oldProductRotation;
    private Vector3 _oldProductScale;
    private Vector3 _offset;

    private float _zoomUpLimit;
    private float _zoomDownLimit;

    
    void Start()
    {
        _offset = new Vector3(0, 1, 3);
        _zoomUpLimit = 1.2f;
        _zoomDownLimit = .5f;
    }
    
    void Update()
    {
        transform.position = playerCamera.transform.position - _offset;
    }

    /// <summary>
    /// a product is selected
    /// </summary>
    /// <param name="selectedObject">selected gameobject</param>
    public void ProductSelected(GameObject selectedObject)
    {
        if (IsProductSelected())
            return;
        
        _oldProductPosition = selectedObject.transform.position;
        _oldProductRotation = selectedObject.transform.rotation;
        _oldProductScale = selectedObject.transform.localScale;

        selectedObject.transform.SetParent(closeView);
        selectedObject.transform.localPosition = Vector3.zero;

        ActivateOptions();
    }

    /// <summary>
    /// activate the product options
    /// </summary>
    void ActivateOptions()
    {
        option.SetActive(true);
    }

    /// <summary>
    /// Rotate the product
    /// </summary>
    /// <param name="direction">rotate directon</param>
    public void RotateProduct(Vector3 direction)
    {
        if (!IsProductSelected())
            return;

        closeView.GetChild(0).Rotate(direction);
    }

    /// <summary>
    /// Scale the product
    /// </summary>
    /// <param name="value">true denotes scale up</param>
    public void ScaleProduct(bool value)
    {
        if (value && closeView.GetChild(0).transform.localScale.y < _zoomUpLimit)
            closeView.GetChild(0).transform.localScale += new Vector3(.05f, .05f, .05f);
        else if (!value && closeView.GetChild(0).transform.localScale.y > _zoomDownLimit)
            closeView.GetChild(0).transform.localScale -= new Vector3(.05f, .05f, .05f);
    }

    /// <summary>
    /// return the product
    /// </summary>
    public void ReturnProduct()
    {
        if (!IsProductSelected())
            return;

        //only for first scene
        if (closeView.GetChild(0).name == "Cube")
            ActivateMovement();

        Transform product = closeView.GetChild(0);

        product.parent = null;
        product.position = _oldProductPosition;
        product.rotation = _oldProductRotation;
        product.localScale = _oldProductScale;

        option.SetActive(false);
    }

    /// <summary>
    /// is any product selected
    /// </summary>
    /// <returns></returns>
    public bool IsProductSelected()
    {
        if (closeView.childCount > 0)
            return true;
        else
            return false;
    }

    /// <summary>
    /// activate the player movement
    /// </summary>
    void ActivateMovement()
    {
        player.enabled = true;
    }

}
