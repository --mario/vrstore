﻿using UnityEngine;

public class TutorialCamera : CameraBehaviour {
    
    public CloseUp closeUp;    

    void Start()
    {
        _mainCam = GetComponent<Camera>();
        closeUp = FindObjectOfType<CloseUp>();
    }
    
    void LateUpdate()
    {
        if (currentObject != null)
            Hovered();
    }

    void Hovered()
    {
        if (currentObject.tag == "Icon")
        {
            CommonHoverOptions(currentObject);
            ExtendedHoverOptions(currentObject);
        }
        else if (currentObject.tag == "Product" && _isClick && canMakeSelection)
            closeUpScript.ProductSelected(currentObject);
    }

    void ExtendedHoverOptions(GameObject button)
    {
        switch (button.name)
        {
            case "dismiss":
                {
                    if (_isClick)
                        closeUp.ChangeMessage();
                }
                break;
        }
    }
}
